#include <stdio.h>

int count_char(const char *str, char character) {
    int count = 0;
    while (*str) {
        if (*str == character) {
            count++;
        }
        str++;
    }
    return count;
}

int main() {
    char str[100];
    char character;

    printf("Entrez une chaîne de caractères : ");
    fgets(str, sizeof(str), stdin);

    printf("Entrez le caractère à compter : ");
    scanf("%c", &character);

    int result = count_char(str, character);
    printf("Le caractère '%c' apparaît %d fois dans la chaîne.\n", character, result);

    return 0;
}
