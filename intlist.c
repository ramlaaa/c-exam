#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;
struct Node {
    int value;
    Node *next;
};

void print_list(Node *head) {
    Node *current;

    current = head;
    while (current) {
        printf("%d ", current->value);
        current = current->next;
    }
    printf("\n");
}

Node *new_node(int value) {
    Node *node;
    node = malloc(sizeof(Node));
    node->value = value;
    node->next = NULL;
    return node;
}

Node *append_value(Node *head, int value) {
    Node *current;

    current = head;

    if (head == NULL) {
        head = new_node(value);
    } else {
        current = head;
        while (current->next) {
            current = current->next;
        }
        current->next = new_node(value);
    }
    return head;
}

int length(Node *head) {
    Node *current;
    int len = 0;

    current = head;
    while (current) {
        len++;
        current = current->next;
    }
    return len;
}

int total(Node *head) {
    Node *current;
    int sum = 0;

    current = head;
    while (current) {
        sum += current->value;
        current = current->next;
    }
    return sum;
}

int main() {
    int N;
    Node *head = NULL;

    head = append_value(head, 42);
    head = append_value(head, 4);
    head = append_value(head, 5);
    head = append_value(head, 42);
    head = append_value(head, 1);

    print_list(head);
    printf("Longueur de la liste : %d\n", length(head));
    printf("Somme des valeurs de la liste : %d\n", total(head));

    exit(0);
}
