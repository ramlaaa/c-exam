#include <stdio.h>

int compte_int(const int *tableau, int taille, int valeur) {
    int count = 0;
    for (int i = 0; i < taille; i++) {
        if (tableau[i] == valeur) {
            count++;
        }
    }
    return count;
}

int main() {
    int taille;
    printf("Entrez la taille du tableau : ");
    scanf("%d", &taille);

    int tableau[taille];
    printf("Entrez les éléments du tableau :\n");
    for (int i = 0; i < taille; i++) {
        scanf("%d", &tableau[i]);
    }

    int valeur;
    printf("Entrez la valeur à compter : ");
    scanf("%d", &valeur);

    int result = compte_int(tableau, taille, valeur);
    printf("La valeur %d apparaît %d fois dans le tableau.\n", valeur, result);

    return 0;
}
