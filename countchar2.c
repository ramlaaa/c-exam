#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

int count_char(const char *str, char character, bool case_insensitive) {
    int count = 0;
    char target = case_insensitive ? tolower(character) : character;
    
    while (*str) {
        char current = case_insensitive ? tolower(*str) : *str;
        if (current == target) {
            count++;
        }
        str++;
    }
    return count;
}

int main() {
    char str[100];
    char character;
    char option;

    printf("Entrez une chaîne de caractères : ");
    fgets(str, sizeof(str), stdin);

    printf("Entrez le caractère à compter : ");
    scanf(" %c", &character);

    printf("Voulez-vous un comptage insensible à la casse ? (y/n) ");
    scanf(" %c", &option);
    bool case_insensitive = (option == 'y' || option == 'Y');

    int result = count_char(str, character, case_insensitive);
    printf("Le caractère '%c' apparaît %d fois dans la chaîne.\n", character, result);

    return 0;
}
