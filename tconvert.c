#include <stdio.h>

float celsius2fahrenheit(float celsius) {
    return (9/5.0) * celsius + 32;
}

float fahrenheit2celsius(float fahrenheit) {
    return (fahrenheit - 32) * 5/9.0;
}

int main() {
    int choice;
    float temperature;

    printf("Choisissez le type de conversion :\n");
    printf("1. Conversion de Celsius vers Fahrenheit\n");
    printf("2. Conversion de Fahrenheit vers Celsius\n");

    scanf("%d", &choice);

    if (choice == 1) {
        printf("Entrez la température en degrés Celsius : ");
        scanf("%f", &temperature);
        float fahrenheit = celsius2fahrenheit(temperature);
        printf("%.2f degrés Celsius équivalent à %.2f degrés Fahrenheit.\n", temperature, fahrenheit);
    }
    else if (choice == 2) {
        printf("Entrez la température en degrés Fahrenheit : ");
        scanf("%f", &temperature);
        float celsius = fahrenheit2celsius(temperature);
        printf("%.2f degrés Fahrenheit équivalent à %.2f degrés Celsius.\n", temperature, celsius);
    }
    else {
        printf("Choix invalide. Veuillez saisir 1 ou 2.\n");
    }

    return 0;
}
